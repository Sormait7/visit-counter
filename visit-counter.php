<?php
/**
 * Plugin Name: Visit counter
 * Description: Count the post vivits
 * Version:     1.0.0
 * Author:      Sparkling
 */

// ini_set( 'display_errors', 1 );
// error_reporting( E_ALL );

defined( 'ABSPATH' ) or die();
define( 'SVCDIR', dirname( __FILE__ ) );
define( 'SVCURL', plugin_dir_url( __FILE__ ) );

require_once( SVCDIR.'/inc/filters.php' );
require_once( SVCDIR.'/inc/actions.php' );
require_once( SVCDIR.'/inc/scripts.php' );
require_once( SVCDIR.'/inc/functions.php' );