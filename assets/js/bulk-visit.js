/**
 * getting bulk posts views data from specified in plugin setting service
 */
( function( $ ){
	$( document ).ready( function(){
		var table = $( '.wp-list-table.posts' );
		var collection;
		var list = [];

		if( table.length ){
			collection = $( '[data-svc-id]' );

			for ( let i = 0; i < collection.length; i++ ) {
				list.push( $( collection[i] ).attr( 'data-svc-url' ) )
			}

			$.ajax( {
				method: 'POST',
				url: svc_url,
				data: {
					name: svc_name,
					urls: list,
				}
			} ).done( function( r ){
				r = JSON.parse( r );
				if( r instanceof Array ){
					for ( let i in r ){
						$( '[data-svc-url="'+r[i].url+'"]' ).text( r[i].visit )
					}	
				}
			} );
		}// if
	} );// ready
} )( jQuery );