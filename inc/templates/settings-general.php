<?php 
/**
 * layout for general options for plugin
 */
defined( 'ABSPATH' ) or die(); 
?>

<div class="wrap">
<h2>Visit counter settings</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'svc-general' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Service url</th>
        <td><input type="url" name="svc_service_url" value="<?php echo get_option( 'svc_service_url' ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Name</th>
        <td><input type="text" name="svc_request_name_option" value="<?php echo get_option( 'svc_request_name_option' ); ?>" /></td>
        </tr>
    </table>
    
    <p class="submit">
    <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>

</form>
<p>To get information from the code you can use function <code>svc_get_visits()</code>. It takes array with filter conditions. Name is filled automatically from form above.</p>
</div>