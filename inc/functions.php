<?php
/**
 * plugin common functions
 *
 * @author Paul Strelkovsky
 */
defined( 'ABSPATH' ) or die();

/**
 * function for rendering general setting page for plugin
 *
 * @see inc/actions.php svc_plugin_options() function
 */
function svc_render_settings_page() {
	require_once( SVCDIR.'/inc/templates/settings-general.php' );
}

/**
 * getting information from service specified in plugin settings
 * by conditions
 *
 * @param array $params - url, last_visit, slug, limit
 * @return array - set of records about appropriate urls
 */
function svc_get_visits( $params ){
	// url required
	if( !isset( $params['url'] ) || empty( $params['url'] ) ){
		return [ 'status' => false, 'message' => 'Urls too short or empty' ];
	}

	// $service_url = get_option( 'svc_service_url' ).'/get';
	$service_url = 'http://data.actualsolutions.net';
	$params['name'] = get_option( 'svc_request_name_option' );

	// doing request
	$curl = curl_init();
	curl_setopt( $curl, CURLOPT_URL, $service_url );
	curl_setopt( $curl, CURLOPT_HEADER, 1 );
	curl_setopt( $curl, CURLOPT_POST, 1 );
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $curl, CURLOPT_POSTFIELDS, $params );
	$res = curl_exec( $curl );
	
	if( !$res ) {
		$resp =  [ 'status' => false, 'message' => curl_error( $curl ) ];
	}
	else {
		$resp = json_decode( $res );
	}

	curl_close($curl);
	return $resp;
}