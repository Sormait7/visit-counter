<?php
/**
 * filters for plugin
 *
 * @author Paul Strelkovsky
 */
defined( 'ABSPATH' ) or die();

/**
 * adding column description to posts admin table
 */
add_filter( 'manage_post_posts_columns', 'svc_add_views_column', 4 );
function svc_add_views_column( $columns ){
	$num = 6;

	$new_columns = array(
		'views' => 'Visit',
	);

	return array_slice( $columns, 0, $num ) + $new_columns + array_slice( $columns, $num );
}