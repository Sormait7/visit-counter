<?php 
/**
 * actions for plugin
 *
 * @author Paul Strelkovsky
 */
defined( 'ABSPATH' ) or die();

/**
 * filling cells in post views column
 */
add_action('manage_post_posts_custom_column', 'svc_fill_views_column', 5, 2 );
function svc_fill_views_column( $colname, $post_id ){
	if( $colname === 'views' ){
		// echo get_post_meta( $post_id, 'views', 1 ); 
		$url = get_permalink( $post_id );
		echo '<p class="svc-views-count" data-svc-url="'.$url.'" data-svc-id="'.$post_id.'"><span class="svc-wait"></span></p>'; 
	}
}

/**
 * adding link for the plugin options page into menu 'settings'
 */
add_action('admin_menu', 'svc_plugin_options');
function svc_plugin_options() {
	add_options_page( 'Visit counter settings', 'Visit counter', 'administrator', 'svc-settings', 'svc_render_settings_page' );
}

/**
 * registering plugin settings
 */
add_action( 'admin_init', 'svc_register_settings' );
function svc_register_settings() {
	register_setting( 'svc-general', 'svc_service_url' );
	register_setting( 'svc-general', 'svc_request_name_option' );
}