<?php
/**
 * registration scripts and styles for plugin purposes
 *
 * @author Paul Strelkovsky
 */
defined( 'ABSPATH' ) or die(); 

/**
 * adding scripts and styles to front of site
 */
add_action( 'wp_enqueue_scripts', 'svc_enqueue_sctipsts' );
function svc_enqueue_sctipsts(){
	if( is_singular( 'post' ) ){
		wp_enqueue_script( 'svc-visit', SVCURL .'assets/js/visit.js', ['jquery'], '1.0', true );
		$data = 'var svc_slug = "'.get_post()->post_name.'";';
		$data .= 'var svc_name = "'.get_option( 'svc_request_name_option' ).'";';
		$data .= 'var svc_url = "'.get_option( 'svc_service_url' ).'/add";';
		wp_add_inline_script( 'svc-visit', $data, 'before' );
	}
}

/**
 * enqueue script for admin area
 *
 * - getting bulk post views data from specified in plugin options service
 */
add_action( 'admin_enqueue_scripts', 'svc_admin_scripts' );
function svc_admin_scripts( $hook_suffix ){
	/**
	 * script will be able only on edit.php screen
	 */
	if( $hook_suffix == 'edit.php' ){
		wp_enqueue_script( 'svc-admin-visit', SVCURL.'assets/js/bulk-visit.js', ['jquery'], '1.0', true );
		$data = 'var svc_name = "'.get_option( 'svc_request_name_option' ).'";';
		$data .= 'var svc_url = "'.get_option( 'svc_service_url' ).'/get/bulk";';
		wp_add_inline_script( 'svc-admin-visit', $data, 'before' );

		wp_enqueue_style( 'svc-admin-visit', SVCURL.'assets/css/bulk-visit.css' );
	}
}